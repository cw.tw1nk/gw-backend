import { join } from "path";
import { loggerConfig } from "./logger";
import mongooseConfig from "./mongoose";
import jwt_config from "./jwt_config";
import { FileSyncAdapter } from "@tsed/adapters";

const { version } = require("../../package.json");
export const rootDir = join(__dirname, "..");

export const config: Partial<TsED.Configuration> = {
	version,
	rootDir,
	logger: loggerConfig,
	mongoose: mongooseConfig,
	adapters: [
		FileSyncAdapter
	],
	jwt: jwt_config,
	glot_token: '20c5bf0f-c2a9-44cc-bd60-5fd1ebd14c34'
	// additional shared configuration
};
