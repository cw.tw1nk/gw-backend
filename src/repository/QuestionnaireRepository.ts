import {Inject, Injectable} from "@tsed/di";
import {Questionnaire} from "@models/questionnaire/Questionnaire";
import {MongooseModel} from "@tsed/mongoose";
import {Repository} from "./Repository";

@Injectable()
export class QuestionnaireRepository extends Repository<Questionnaire>{
  @Inject(Questionnaire)
  protected model: MongooseModel<Questionnaire>;
}