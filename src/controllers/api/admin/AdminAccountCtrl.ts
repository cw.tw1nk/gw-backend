import { Controller, Inject } from "@tsed/di";
import { AdminAccountRepository } from "@repository/AdminAccountRepository";
import { BodyParams, Delete, PathParams, Post, QueryParams } from "@tsed/common";
import { ContentType, Get, Returns } from "@tsed/schema";
import { Authorize } from "@tsed/passport";
import { Token } from "@models/Token";
import { StatusType } from "@models/StatusType";
import { AdminAccountCreation } from "@models/admin/AdminAccountCreation";
import { AdminAccount } from "@models/admin/AdminAccount";
import { Pagination } from "@models/Page";
import { Form } from "@models/form/Form";

@Controller("/admin-account")
@Authorize("admin-jwt")
export class AdminAccountCtrl {

	@Inject(AdminAccountRepository)
	repository: AdminAccountRepository;

	@Post("/create-test-account")
	@ContentType("json")
	@Returns(200, Token)
	@Returns(401)
	async createTestAccount() {
		return this.repository.create({email: 'demo@demo.com', password: 'demo12', role: 'admin', firstName: 'demo', lastName: 'demo', status: StatusType.ACTIVE})
	}

	@Post("/")
	@ContentType("json")
	@Returns(200, Form)
	@Returns(401)
	async save(@BodyParams() value: AdminAccountCreation) {
		return this.repository.save(value)
	}

	@Delete("/")
	@ContentType("json")
	@Returns(200)
	async deleteAccount(@QueryParams() id: string) {
		await this.repository.deleteById(id)
	}

	@Get("/:id")
	@ContentType("json")
	@Returns(200, AdminAccount)
	@Returns(401)
	async get(@PathParams('id') id: string) {
		return this.repository.findById(id);
	}

	@Get("/")
	@ContentType("json")
	@Returns(200, Pagination).of(AdminAccount)
	@Returns(401)
	async list(@QueryParams() skip: number, @QueryParams() limit: number, @QueryParams() sort: string) {
		/*
			limit:
				field -- asc
				-field -- desc
			example: first_name -last_name
		*/
		return this.repository.list(skip, limit).sort(sort).exec()
	}
}
