import { Inject, Service } from "@tsed/di";
import { ProjectRepository } from "@repository/ProjectRepository";
import { QuestionnaireRepository } from "@repository/QuestionnaireRepository";
import { QuestionRepository } from "@repository/QuestionRepostory";
import { InviteRepository } from "@repository/InviteRepository";
import { NotFound } from "@tsed/exceptions";
import { StatusType } from "@models/StatusType";
import { UserSession } from "@models/UserSession";
import { ProjectAnswer } from "@models/Project";
import { QuestionType } from "@models/questionnaire/QuestionType";
import { Ref } from "@tsed/mongoose";
import _ from "lodash";

@Service()
export class ProjectService {
    @Inject(ProjectRepository)
    private project: ProjectRepository;
    @Inject(QuestionnaireRepository)
    private questionnaire: QuestionnaireRepository;
    @Inject(QuestionRepository)
    private question: QuestionRepository;
    @Inject(InviteRepository)
    private invite: InviteRepository;

    async create(id: string, session_id: Ref<UserSession>) {
        let inv = await this.invite.findOne({
            _id: id,
            status: StatusType.ACTIVE,
        });
        if (inv) {
            if (inv?.one_off) {
                await this.invite.update({_id: inv._id}, {status: StatusType.INACTIVE});
            }

            //restore project by session if exist
            let project = await this.project.findOne({
                session: session_id,
                questionnaire: inv?.questionnaire,
                status: StatusType.IN_PROGRESS
            });
            if (project) {
                return project;
            }

            return await this.project.create({
                answers: [],
                grade: 0.0,
                questionnaire: inv?.questionnaire,
                status: StatusType.IN_PROGRESS,
                session: session_id ?? null
            });
        } else {
            throw new NotFound("Invite not found");
        }
    }

    async getCurrentQuestion(project_id: string) {
        let project = await this.project.findById(project_id);
        if (project) {
            if (project.status === StatusType.IN_PROGRESS && project.current_question) {
                return this.question.findById(project.current_question);
            } else {
                return {} //todo
            }
        }
        throw new NotFound("Project not found")
    }

    async saveAnswer(project_id: string, answer: ProjectAnswer) {
        let next;
        const current = await this.question.findById(answer.question);
        let status = StatusType.IN_PROGRESS;
        if (current?.type !== QuestionType.OPTION) {
            if (current?.next_question) {
                let next_q = await this.question.findById(current.next_question);
                next = next_q?._id;
            } else {
                next = null;
            }
        } else {
            if (answer.options && answer.options.length === 1) {
                let answer_option = await this.question.findAnswer(answer.options[0]);
                if (answer_option) {
                    if (answer_option.next_question) {
                        next = answer_option.next_question;
                    }
                }
            }
        }

        if (!next) {
            status = StatusType.COMPLETED;
        }

        answer.is_valid = current?.toClass().checkAnswer(answer);
        let value = await this.project.findById(project_id);
        if (value) {
            value.answers = _.unionBy([answer], value.answers, 'question');
            value.prev_question = answer.question;
            value.current_question = next;
            value.status = status;
            await value.save()
            // await this.project.updateOne({_id: project_id}, {
            //     $push: {answers: answer},
            //     $set: {prev_question: answer.question, current_question: next, status: status}
            // });
        }


        if (status === StatusType.COMPLETED) {
            return {}; //todo results
        }
        return await this.question.findById(next);
    }

}

