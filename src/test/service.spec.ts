import { PlatformTest } from "@tsed/common";
import { ProjectService } from "../services/ProjectService";
import { GlotLanguage, GlotService } from "../services/GlotService";

describe("GlotService", () => {
    beforeEach(PlatformTest.create);
    afterEach(PlatformTest.reset);
    jest.setTimeout(20000);
    it("runCode", async () => {
        let glotService = PlatformTest.get<GlotService>(GlotService);
        console.log(await glotService.runCode("", GlotLanguage.PYTHON))
        // let qr = PlatformTest.get<QuestionRepository>(QuestionRepository);
        // // let invite = await projectService.createInvite();
        // // console.log("invite", invite);
        // // projectService.
        // console.log(await qr.findAnswer("6241d384fe542dc699c12f7a"));
    })
})