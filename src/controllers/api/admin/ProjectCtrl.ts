import { Controller, Inject } from "@tsed/di";
import { Authorize } from "@tsed/passport";
import { ProjectRepository } from "@repository/ProjectRepository";
import { Delete, PathParams, QueryParams } from "@tsed/common";
import { ContentType, Get, Returns } from "@tsed/schema";
import { Project } from "@models/Project";
import { Pagination } from "@models/Page";

@Controller("/project")
@Authorize("admin-jwt")
export class ProjectCtrl {

	@Inject(ProjectRepository)
	repository: ProjectRepository;

	@Delete("/")
	@ContentType("json")
	@Returns(200)
	async removeInvite(@QueryParams() id: string) {
		await this.repository.deleteById(id)
	}

	@Get("/:id")
	@ContentType("json")
	@Returns(200, Project)
	@Returns(401)
	async get(@PathParams('id') id: string) {
		return this.repository.findById(id);
	}

	@Get("/")
	@ContentType("json")
	@Returns(200, Pagination).of(Project)
	@Returns(401)
	async list(@QueryParams() skip: number, @QueryParams() limit: number, @QueryParams() sort: string) {
		/*
			field -- asc
			-field -- desc
		*/
		return this.repository.list(skip, limit).sort(sort).exec()
	}

}
