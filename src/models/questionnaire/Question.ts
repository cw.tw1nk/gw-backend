import { CollectionOf, Default, Description, Enum, Example, Integer, Nullable, Property } from "@tsed/schema";
import { Model, ObjectID, Ref, Schema } from "@tsed/mongoose";
import { QuestionType } from "./QuestionType";
import _, { toString } from "lodash";
import { ProjectAnswer } from "../Project";
import { Form } from "@models/form/Form";


@Schema()
export class QuestionAnswer {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id: string;

	@Property()
	title: string;

	@Property()
	value: string;

	@Integer()
	order: number = 0;

	@Property()
	valid: boolean;

	@Ref(() => Question)
	@Property()
	next_question?: Ref<Question>;
}

@Model()
export class Question {
	@ObjectID("id")
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Property()
	title: string;

	@Property()
	content: string;

	@Ref(Form)
	@Nullable
	form_ref: Ref<Form> | null = null;

	@Ref(Question)
	@Nullable
	next_question?: Ref<Question> | null = null;

	@Enum(QuestionType)
	type: QuestionType = QuestionType.INPUT;

	@CollectionOf(QuestionAnswer)
	@Default([])
	answers: QuestionAnswer[] = [];

	@Integer()
	@Default(0)
	depth: number = 0;

	@Integer()
	@Default(0.0)
	filling_percentage?: number = 0.0;

	@Integer()
	@Default(0.0)
	grade?: number = 0.0;

	@Property()
	@Default(true)
	result_show: boolean = true;

	@Property()
	@Default(false)
	required: boolean = false;

	checkAnswer(value: Omit<ProjectAnswer, '_id' | 'is_valid'>): boolean {
		if (this.type === QuestionType.OPTION) {
			return _.some(this.answers, {_id: _.map(value.options, toString), valid: true})
		} else if (this.type === QuestionType.CHECKBOX) {
			return _(this.answers).filter({valid: true}).map('_id').sortBy().isEqual(_(value.options).map(toString).sortBy())
		}
		//todo check value.answer
		return true;
	}

	// validate(value: number | string | string[]): boolean {
	// 	if (this.type === QuestionType.OPTION) {
	// 		if (Array.isArray(value)) {
	// 			if (value.length > 0) {
	// 				value = value[0];
	// 			}
	// 		}
	// 		return _.some(this.answers, {_id: value, valid: true})
	// 	} else if (this.type === QuestionType.CHECKBOX) {
	// 		if (!Array.isArray(value) && value) {
	// 			value = [value as string]
	// 		}
	// 		return _(this.answers).filter({valid: true}).map('_id').sort().isEqual(_.sortBy(value as string[]))
	// 	} else {
	// 		//todo
	// 		return true;
	// 	}
	// }
}

