import {BodyParams, HeaderParams, Req} from '@tsed/common';
import {Arg, OnVerify, Protocol} from '@tsed/passport';
import {ExtractJwt, Strategy, StrategyOptions} from 'passport-jwt';
import * as moment from 'moment';
import * as jwt from 'jsonwebtoken';
import config from '@config/jwt_config'
import { AdminAccount } from "@models/admin/AdminAccount";
import { AdminAccountRepository } from "@repository/AdminAccountRepository";
@Protocol<StrategyOptions>({
	name: 'admin-jwt',
	useStrategy: Strategy,
	settings: {
		jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
		secretOrKey: config.secret_key,
		issuer: config.issuer,
		audience: config.audience,
		ignoreExpiration: false,
	},
})
export class JwtProtocol implements OnVerify {
	constructor(private accountRepository: AdminAccountRepository) {}

	async $onVerify(
		@Req() req: Req,
		@Arg(0) jwtPayload: {sub: string; exp: number; iat: number; ip: string; iss: string; aud: string},
		@HeaderParams('authorization') token: string
	): Promise<AdminAccount|false> {
		if (jwtPayload.ip === req.ip) {
			const user = await this.accountRepository.findById(jwtPayload.sub);
			return user ? user : false;
		}
		return false;
	}
}
