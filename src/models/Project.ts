import {
	AdditionalProperties,
	Any,
	CollectionOf,
	Default,
	Description,
	Enum,
	Example, Integer,
	Nullable,
	Property, Required
} from "@tsed/schema";
import { Model, ObjectID, Ref, Schema, Unique } from "@tsed/mongoose";
import { Question, QuestionAnswer } from "./questionnaire/Question";
import {Questionnaire} from "./questionnaire/Questionnaire";
import {StatusType} from "./StatusType";
import {UserSession} from "./UserSession";
import { Field } from "@models/form/Form";

export class FormAnswer {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Ref(Field)
	@Required()
	field: Ref<Field>

	@Any(String, Number)
	value: string | number
}

@Schema()
export class ProjectAnswer {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Ref(Question)
	@Unique()
	@Required()
	question: Ref<Question>;

	@Ref(QuestionAnswer)
	@Default([])
	options: Ref<QuestionAnswer>[] = [];

	@Property(FormAnswer)
	@Nullable
	form?: FormAnswer | null = null;

	@Any(String, Number)
	@Default(-1)
	@Nullable
	answer?: string | number | null;

	@Property()
	@Default(true)
	is_valid?: boolean = true;
}

@Model()
@AdditionalProperties(true)
export class Project {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Ref(UserSession)
	session?: Ref<UserSession>;

	@CollectionOf(ProjectAnswer)
	@Default([])
	answers: ProjectAnswer[] = [];

	@Enum()
	status: StatusType = StatusType.IN_PROGRESS

	@Ref(Question)
	@Nullable
	prev_question?: Ref<Question>;

	@Ref(Question)
	@Nullable
	current_question?: Ref<Question>

	@Ref(Questionnaire)
	questionnaire?: Ref<Questionnaire>

	@Integer()
	@Default(0.0)
	grade?: number = 0.0;

	[key: string]: unknown;
}
