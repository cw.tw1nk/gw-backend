import {BodyParams, Cookies, PathParams, Post, UseBefore} from "@tsed/common";
import { Controller, Inject } from "@tsed/di";
import { Get, Returns } from "@tsed/schema";
import { SessionMiddleware } from "@middleware/SessionMiddleware";
import {ProjectAnswer} from "@models/Project";
import {ProjectService} from "@services/ProjectService";
import {Question} from "@models/questionnaire/Question";


export type ProjectAnswerPayload = Omit<ProjectAnswer, "_id" | "is_valid">;
export type CodeRunPayload = {code: string, language: string}

@Controller("/questionnaire")
@UseBefore(SessionMiddleware)
export class QuestionnaireController {
  @Inject(ProjectService)
  ps: ProjectService;

  @Get("/:invite")
  async createProject(@PathParams("invite") invite: string) {
    // @ts-ignore
    const { session_id } = req.session.user_session;
    return await this.ps.create(invite, session_id)
  }

  @Get("/project/:project_id/question")
  @(Returns(200, Question).ContentType("application/json"))
  getCurrentQuestion(@PathParams("project_id") project_id: string) {
    return this.ps.getCurrentQuestion(project_id); //todo
  }

  @Post("/project/:project_id/question/")
  async saveAnswer(@PathParams("project_id") project_id: string, @BodyParams() body: ProjectAnswerPayload) {
    return await this.ps.saveAnswer(project_id, body);
  }

  @Post('/project/:project_id/question/:question_id')
  async runCode(@PathParams("project_id") project_id: string, @PathParams("question_id") question_id: string, @BodyParams() body: CodeRunPayload) {
    // Axios.post()
  //    todo
  }
}
