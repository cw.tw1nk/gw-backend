import * as jwt from "jsonwebtoken";
import jwtConfig from '@config/jwt_config'
import { JwtPayload } from "jsonwebtoken";

export function generateJwtToken(sub: string, expiresIn: string | number, options: Record<string, unknown> = {}): string {
	const {issuer, audience, secret_key} = jwtConfig;
	return jwt.sign(
		{
			iss: issuer,
			aud: audience,
			sub: sub,
			...options,
		},
		secret_key,
		{
			expiresIn: expiresIn,
			algorithm: 'HS256',
		}
	);
}
export function verifyToken(token: string):Promise<JwtPayload|string> {
	return new Promise<JwtPayload|string>((resolve, reject) => {
		jwt.verify(token, jwtConfig.secret_key, (err, info) => {
			if (err) {
				reject(err.message)
			} else {
				resolve(info!)
			}
		})
	});
}
