import { Model, MongooseIndex, ObjectID } from "@tsed/mongoose";
import { Description, Example, Ignore } from "@tsed/schema";
import { AdminAccountCreation } from "@models/admin/AdminAccountCreation";
import * as bcrypt from 'bcrypt';

@Model()
@MongooseIndex({expires: 1}, {expireAfterSeconds: 0})
export class AdminAccount extends AdminAccountCreation {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id: string;

	@Ignore()
	declare password: string;

	@Ignore()
	declare token: string;
}
