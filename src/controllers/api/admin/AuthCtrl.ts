import { BodyParams, Delete, Post, Req } from "@tsed/common";
import { Controller, Inject } from "@tsed/di";
import { ContentType, Returns } from "@tsed/schema";
import { Authenticate } from "@tsed/passport";
import { Token } from "@models/Token";
import { Credentials } from "@models/admin/Credentials";
import { AdminAccountRepository } from "@repository/AdminAccountRepository";

@Controller("/auth")
export class AuthCtrl {

	@Inject(AdminAccountRepository)
	repository: AdminAccountRepository;

	@Post("/login")
	@ContentType("json")
	@Authenticate("local")
	@Returns(200, Token)
	@Returns(401)
	async login(@Req() req: Req, @BodyParams(Credentials) value: Credentials): Promise<Token> {
		return { token: req.user!.token };
	}

	@Delete("/logout")
	@ContentType("json")
	@Returns(200)
	async logout(@Req() req: Req) {
		req.logout();
	}
}
