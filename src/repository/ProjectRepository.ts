import {Inject, Injectable} from "@tsed/di";
import { MongooseModel, Ref } from "@tsed/mongoose";
import { Project, ProjectAnswer } from "@models/Project";
import {Repository} from "./Repository";
import { Question, QuestionAnswer } from "@models/questionnaire/Question";
import { ObjectId } from "mongoose";

@Injectable()
export class ProjectRepository extends Repository<Project>{
  @Inject(Project)
  protected model: MongooseModel<Project>;

  async findAnswerByQuestion(p_id: string | ObjectId | Ref<Project>, q_id: string | ObjectId | Ref<Question>) {
    // this.model.findOne({'answers._id': new Types.ObjectId(_id)})
    let obj = await this.model.findOne({ _id: p_id, "answers.question": q_id }, { answers: { $elemMatch: { question: q_id } } }).exec();
    if (obj?.answers) {
      if (obj.answers.length === 1) {
        return obj.answers[0];
      }
    }
    return null;
  }
}
