/* eslint-disable prettier/prettier */
import { Default, Description, Email, Example, Format, Name, Property } from "@tsed/schema";
import { Model } from "@tsed/mongoose";

@Model()
export class UserSession {
  @Name("id")
  @Description("Object ID")
  @Example("5ce7ad3028890bd71749d477")
  _id?: string;

  @Property()
  ip?: string;

  @Property()
  entry_url?: string;

  @Property()
  user_agent?: string;

  @Format("date-time")
  @Default(Date.now)
  created_at: Date;


  [key: string]: any;
}
