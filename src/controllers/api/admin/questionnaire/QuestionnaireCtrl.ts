import { Controller, Inject } from "@tsed/di";
import { Authorize } from "@tsed/passport";
import { QuestionnaireRepository } from "@repository/QuestionnaireRepository";
import { BodyParams, Delete, PathParams, Post, QueryParams } from "@tsed/common";
import { ContentType, Get, Returns } from "@tsed/schema";
import { Pagination } from "@models/Page";
import { Questionnaire } from "@models/questionnaire/Questionnaire";

@Controller("/questionnaire")
@Authorize("admin-jwt")
export class QuestionnaireCtrl {

	@Inject(QuestionnaireRepository)
	repository: QuestionnaireRepository;

	@Post("/")
	@ContentType("json")
	@Returns(200, Questionnaire)
	@Returns(401)
	async save(@BodyParams() value: Questionnaire) {
		return this.repository.save(value)
	}

	@Delete("/")
	@ContentType("json")
	@Returns(200)
	async removeInvite(@QueryParams() id: string) {
		await this.repository.deleteById(id)
	}

	@Get("/:id")
	@ContentType("json")
	@Returns(200, Questionnaire)
	@Returns(401)
	async get(@PathParams('id') id: string) {
		return this.repository.findById(id);
	}

	@Get("/")
	@ContentType("json")
	@Returns(200, Pagination).of(Questionnaire)
	@Returns(401)
	async list(@QueryParams() skip: number, @QueryParams() limit: number, @QueryParams() sort: string) {
		/*
			field -- asc
			-field -- desc
		*/
		return this.repository.list(skip, limit).sort(sort).exec()
	}
}
