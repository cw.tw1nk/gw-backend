export type JWTConfig = { secret_key: string; audience: string; issuer: string }
const config: JWTConfig = {
	secret_key: process.env.JWT_KEY || '2855CA08-7FC5-4890-A2A3-DD9F4FF1A096',
	issuer: 'site.com',
	audience: 'site.com',
};
export default config
