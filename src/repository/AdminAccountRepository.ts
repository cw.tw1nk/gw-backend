import { Repository } from "@repository/Repository";
import { AdminAccount } from "@models/admin/AdminAccount";
import { MongooseModel } from "@tsed/mongoose";
import { Inject, Injectable } from "@tsed/di";
import { AdminAccountCreation } from "@models/admin/AdminAccountCreation";
import * as bcrypt from 'bcrypt';
import _ from "lodash";

@Injectable()
export class AdminAccountRepository extends Repository<AdminAccount>{
	@Inject(AdminAccount)
	protected model: MongooseModel<AdminAccount>;

	async create(value: Partial<AdminAccountCreation>): Promise<AdminAccount> {
		value.password = await bcrypt.hash(value.password!, 10);
		return _.omit(super.create(value), 'password');
	}
}
