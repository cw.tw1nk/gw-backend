import {AdditionalProperties, CollectionOf, Description, Enum, Example, Name, Property} from "@tsed/schema";
import {Model, ObjectID, Ref, Unique} from "@tsed/mongoose";
import {Question} from "./Question";
import {StatusType} from "../StatusType";

@Model()
@AdditionalProperties(true)
export class Questionnaire {
	@ObjectID()
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Property()
	title: string;

	@Property()
	@Unique()
	slug: string;

	@Property()
	description: string = "";

	@Enum(StatusType)
	status: StatusType = StatusType.ACTIVE;

	@Ref(Question)
	first_question: Ref<Question>;

	[key: string]: unknown;
}
