import { Any, CollectionOf, Default, Description, Enum, Example, Integer, Property } from "@tsed/schema";
import { ObjectID, Schema } from "@tsed/mongoose";
import { StatusType } from "@models/StatusType";

enum FieldType {
	INPUT,
	PHONE,
	EMAIL,
	CHECKBOX,
	OPTION,
}

@Schema()
export class Field {
	@ObjectID("id")
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@Property()
	@Default("")
	title: string;

	@Any(String, Number)
	@Default("")
	default_value: string | number;

	@Enum(FieldType)
	type: FieldType = FieldType.INPUT;

	@Property()
	@Default("")
	mask: string;

	@CollectionOf(String)
	@Default([])
	validation: string[] = [];

	@Integer()
	@Default(0)
	order: number = 0;

	@Property()
	@Default(false)
	required: boolean = false;

	@Enum(StatusType)
	status: StatusType = StatusType.ACTIVE;

	//
	// @Enum(QuestionType)
	// type: QuestionType = QuestionType.INPUT;
}

export class Form {
	@ObjectID("id")
	@Description("Object ID")
	@Example("5ce7ad3028890bd71749d477")
	_id?: string;

	@CollectionOf(Field)
	@Default([])
	fields: Field[] = [];

	@Enum(StatusType)
	status: StatusType = StatusType.ACTIVE;
}
