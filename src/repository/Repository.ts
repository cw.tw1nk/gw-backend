import {MongooseModel, Ref} from "@tsed/mongoose";
import {FilterQuery, UpdateQuery, UpdateWithAggregationPipeline} from "mongoose";

export abstract class Repository<T> {
	protected abstract model: MongooseModel<T>;

	async create(value: Partial<Omit<T, "_id">>): Promise<T> {
		const doc = new this.model(value);
		await doc.save();
		return doc;
	}

	async save(value: Partial<T>): Promise<T> {
		const doc = new this.model(value);
		await doc.save();
		return doc;
	}

	async update(query: FilterQuery<T>, value: UpdateQuery<T>) {
		return await this.model.updateMany(query, value).exec();
	}

	async updateOne(filter: FilterQuery<T>, value: UpdateQuery<T> | UpdateWithAggregationPipeline) {
		return await this.model.updateOne(filter, value).exec();
	}

	async findOne(filter: FilterQuery<T>, projection?: any) {
		return await this.model.findOne(filter, projection).exec();
	}

	async exists(filter: FilterQuery<T>) {
		return await this.model.exists(filter).exec();
	}


	async findById(id: Ref<T>) {
		return await this.model.findById(id).exec();
	}

	async find(query?: FilterQuery<T>, projection?: any) {
		if (query) {
			return await this.model.find(query, projection).exec();
		}
		await this.model.find().exec();
	}

	async deleteOne(filter?: FilterQuery<T>) {
		return this.model.deleteOne(filter).exec();
	}

	async deleteMany(filter?: FilterQuery<T>) {
		return this.model.deleteMany(filter).exec();
	}

	async deleteById(id?: Ref<T>) {
		return this.model.findByIdAndDelete(id).exec();
	}

	list(skip: number, limit: number) {
		return this.model.find().skip(skip).limit(limit);
	}

	getModel() {
		return this.model
	}

	//
	// validate(value: T) {
	// 	return this.model.validate(value);
	// }
}
