import { Inject, Injectable } from "@tsed/di";
import { Repository } from "@repository/Repository";
import { Questionnaire } from "@models/questionnaire/Questionnaire";
import { MongooseModel } from "@tsed/mongoose";
import { Form } from "@models/form/Form";

@Injectable()
export class FormRepository extends Repository<Form>{
	@Inject(Form)
	protected model: MongooseModel<Form>;
}
