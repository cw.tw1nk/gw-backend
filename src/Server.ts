import {Configuration, Inject} from "@tsed/di";
import {PlatformApplication} from "@tsed/common";
import "@tsed/platform-express"; ///!\ keep this import
import bodyParser from "body-parser";
import compress from "compression";
import cookieParser from "cookie-parser";
import methodOverride from "method-override";
import cors from "cors";
import "@tsed/ajv";
import "@tsed/swagger";
import "@tsed/mongoose";
import {config, rootDir} from "./config";
import {IndexCtrl} from "@controllers/pages/IndexController";
import helmet from "helmet";
import session from "express-session";
import "@tsed/passport";
import { AdminAccount } from "@models/admin/AdminAccount";

@Configuration({
	...config,
	acceptMimes: ["application/json"],
	componentsScan: [
		`${rootDir}/protocols/**/*.ts`
	],
	passport: {
		userInfoModel: AdminAccount
	},
	httpPort: process.env.PORT || 8083,
	httpsPort: false,
	mount: {
		"/api/admin": [
			`${rootDir}/controllers/api/admin/**/*.ts`
		],
		"/api": [
			`${rootDir}/controllers/api/client/**/*.ts`
		],
		"/": [
			IndexCtrl,
		]
	},
	swagger: [
		{
			path: "/v3/docs",
			specVersion: "3.0.1"
		}
	],
	views: {
		root: `${rootDir}/views`,
		extensions: {
			ejs: "ejs"
		}
	},
	exclude: [
		"**/*.spec.ts"
	]
})
export class Server {
	@Inject()
	app: PlatformApplication;

	@Configuration()
	settings: Configuration;

	$beforeRoutesInit(): void {
		this.app
			.use(helmet({
				contentSecurityPolicy: {
					directives: {
						defaultSrc: [`'self'`],
						styleSrc: [`'self'`, `'unsafe-inline'`],
						imgSrc: [`'self'`, 'data:', 'validator.swagger.io'],
						scriptSrc: [`'self'`, `https: 'unsafe-inline'`],
					},
				},
			}))

			.use(cors())
			.use(cookieParser())
			.use(compress({}))
			.use(methodOverride())
			.use(bodyParser.json())
			.use(bodyParser.urlencoded({
				extended: true
			}));
		// @ts-ignore
		this.app.getApp().set("trust proxy", 1);
		this.app.use(
			session({
				secret: "todokey",
				resave: true,
				saveUninitialized: true,
				// maxAge: 36000,
				cookie: {
					path: "/",
					httpOnly: true,
					secure: false,
				}
			})
		);
	}
}
