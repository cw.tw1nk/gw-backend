import { Context, Cookies, Middleware, Req, Res, Value } from "@tsed/common";
import { Inject } from "@tsed/di";
import { SessionRepository } from "@repository/SessionRepository";
import moment from "moment";
import { verifyToken, generateJwtToken } from "@app/utils/jwt";

type Session = { [key: string]: any };

@Middleware()
export class SessionMiddleware {

	@Inject(SessionRepository)
	sr: SessionRepository;

	async use(@Req() req: Req, @Res() res: Res, @Context() ctx: Context, @Cookies() cookies: any) {
		let session = <Record<string, any>>req.session;
		if (!session.user_session && typeof cookies.token == "string") {
			let jwt = await verifyToken(cookies.token);
			if (jwt) {
				session.user_session = {
					token: cookies.token,
					session_id: jwt?.sub
				};
			}
		} else {
			await this.createSession(req, session, res);
		}
	}

	private async createSession(req: Req, session: Record<string, any>, res: Res) {
		let session_id = (await this.sr.create({
			created_at: Date.now(),
			ip: req.ip,
			user_agent: req.header("User-Agent")
		}))._id!;
		session.user_session = {
			token: generateJwtToken(session_id, "30d"),
			session_id
		};
		res.cookie("token", session.user_session.token, { httpOnly: true, expires: moment().add(30, "d").toDate() });
	}
}
