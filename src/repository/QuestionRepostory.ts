import {Inject, Injectable} from "@tsed/di";
import {MongooseModel, Ref} from "@tsed/mongoose";
import {Question, QuestionAnswer} from "@models/questionnaire/Question";
import {Repository} from "./Repository";
import {ObjectId} from "mongoose";

@Injectable()
export class QuestionRepository extends Repository<Question> {
	@Inject(Question)
	protected model: MongooseModel<Question>;

	//
	async findAnswer(_id: string | ObjectId | Ref<QuestionAnswer>) {
		// this.model.findOne({'answers._id': new Types.ObjectId(_id)})
		let obj = await this.model.findOne({ "answers._id": _id }, { answers: { $elemMatch: { _id } } }).exec();
		if (obj?.answers) {
			if (obj.answers.length === 1) {
				return obj.answers[0];
			}
		}
		return null;
	}

	async findAnswerInQuestion(q_id: string, a_id: string) {
		return await this.model.find({ _id: q_id }, { "answers": { $elemMatch: { _id: a_id } } }).exec();
	}
}
