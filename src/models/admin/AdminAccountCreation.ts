import { Credentials } from "@models/admin/Credentials";
import { DateTime, Default, Description, Enum, Property, Required } from "@tsed/schema";
import { StatusType } from "@models/StatusType";

export class AdminAccountCreation extends Credentials {
	@Description("User first name")
	@Required()
	firstName: string;

	@Description("User last name")
	@Required()
	lastName: string;

	@Property()
	@Default('admin')
	role: string = 'admin'

	@DateTime()
	@Default(Date.now)
	created_at: Date = new Date();

	@DateTime()
	@Default(0)
	expires?: Date|number = 0;

	@Enum(StatusType)
	status: StatusType = StatusType.ACTIVE;
}
