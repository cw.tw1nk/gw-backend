// import 'module-alias/register';
import {$log} from "@tsed/common";
import {PlatformExpress} from "@tsed/platform-express";
import {Server} from "./Server";
import {isProduction} from "@config/env";
import localtunnel from "localtunnel";

async function bootstrap() {
  try {
    $log.debug("Start server...");
    const platform = await PlatformExpress.bootstrap(Server);
    await platform.listen();
    if (!isProduction) {
      $log.debug('Init localtunnel')
      const tunnel = await localtunnel(parseInt(process.env.PORT || 8083), {subdomain: 'bhfadhvkajfafjhbadk65fhbsd' });
      $log.info(`URL: ${tunnel.url}, PORT: ${process.env.PORT || 8083}`)
    }
    $log.debug("Server initialized");
  } catch (er) {
    $log.error(er);
  }
}

bootstrap();
