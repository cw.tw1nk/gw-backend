import { Context, Middleware, Req, UseBefore } from "@tsed/common";
import { Unauthorized } from "@tsed/exceptions";
import { StoreSet, useDecorators } from "@tsed/core";

@Middleware()
export class AcceptRolesMiddleware {
	use(@Req() request: Req, @Context() ctx: Context) {

		if (request.user && request.isAuthenticated()) {
			const roles = ctx.endpoint.get(AcceptRolesMiddleware);

			if (!roles.includes(request.user.role)) {
				throw new Unauthorized("Insufficient role");
			}
		}
	}
}

export function AcceptRoles(...roles: string[]) {
	return useDecorators(UseBefore(AcceptRolesMiddleware), StoreSet(AcceptRolesMiddleware, roles));
}
