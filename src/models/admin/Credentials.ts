import { Description, Example, Format, Required } from "@tsed/schema";
import { Unique } from "@tsed/mongoose";

export class Credentials {
	@Description("User email")
	@Example("user@domain.com")
	@Format("email")
	@Required()
	@Unique(true)
	email: string;

	@Description("User password")
	@Example("/5gftuD/")
	@Required()
	password: string;
}
