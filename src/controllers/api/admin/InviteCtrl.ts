import { Controller, Inject } from "@tsed/di";
import { Authorize } from "@tsed/passport";
import { BodyParams, Delete, PathParams, Post, QueryParams } from "@tsed/common";
import { ContentType, Get, Returns } from "@tsed/schema";
import { InviteRepository } from "@repository/InviteRepository";
import { Invite } from "@models/Invite";
import { Pagination } from "@models/Page";
import { Form } from "@models/form/Form";

@Controller("/invite")
@Authorize("admin-jwt")
export class InviteCtrl {

	@Inject(InviteRepository)
	repository: InviteRepository;

	@Post("/")
	@ContentType("json")
	@Returns(200, Form)
	@Returns(401)
	async save(@BodyParams() invite: Invite) {
		return this.repository.save(invite)
	}

	@Delete("/")
	@ContentType("json")
	@Returns(200)
	async removeInvite(@QueryParams() id: string) {
		await this.repository.deleteById(id)
	}

	@Get("/:id")
	@ContentType("json")
	@Returns(200, Invite)
	@Returns(401)
	async get(@PathParams('id') id: string) {
		return this.repository.findById(id);
	}

	@Get("/")
	@ContentType("json")
	@Returns(200, Pagination).of(Invite)
	@Returns(401)
	async list(@QueryParams() skip: number, @QueryParams() limit: number, @QueryParams() sort: string) {
		/*
			field -- asc
			-field -- desc
		*/
		return this.repository.list(skip, limit).sort(sort).exec()
	}
}
