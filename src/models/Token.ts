import { Property } from "@tsed/schema";

export class Token {
	@Property()
	token: string;
}
