declare function parseInt(string: string | number): number

declare namespace Express {
	interface User {
		token: string
		role: string
	}
}
