import {Inject, Injectable} from "@tsed/di";
import {MongooseModel} from "@tsed/mongoose";
import {Repository} from "./Repository";
import {UserSession} from "@models/UserSession";

@Injectable()
export class SessionRepository extends Repository<UserSession> {
	@Inject(UserSession)
	protected model: MongooseModel<UserSession>;
}