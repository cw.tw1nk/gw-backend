import {BodyParams, Inject, Req, Res} from '@tsed/common';
import {OnVerify, Protocol} from '@tsed/passport';
import * as moment from 'moment';
import {IStrategyOptions, Strategy} from 'passport-local';
import { AdminAccountRepository } from "@repository/AdminAccountRepository";
import { Credentials } from '@models/admin/Credentials';
import { Unauthorized } from "@tsed/exceptions";
import { AdminAccount } from "@models/admin/AdminAccount";
import { generateJwtToken } from "@app/utils/jwt";
import * as bcrypt from "bcrypt";

@Protocol<IStrategyOptions>({
	name: 'local',
	useStrategy: Strategy,
	settings: {
		usernameField: 'email',
		passwordField: 'password',
	},
})
export class LocalProtocol implements OnVerify {
	constructor(@Inject() private accountRepository: AdminAccountRepository) {}

	async $onVerify(@Req() request: Req, @Res() response: Res, @BodyParams(Credentials) credentials: Credentials): Promise<AdminAccount> {
		const {email, password} = credentials;
		let account = await this.accountRepository.findOne({email});
		if (account) {
			if (await bcrypt.compare(password, account.password)) {
				account.token = generateJwtToken(account._id, '30d')
				return account;
			}
		}
		throw new Unauthorized("todo")
	}


}
