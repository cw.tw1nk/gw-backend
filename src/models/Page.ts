import { CollectionOf, Generics, Integer, Property } from "@tsed/schema";

@Generics("T")
export class Pagination<T> {
	@Integer()
	totalCount: number;
	@Integer()
	pageSize: number;
	@Integer()
	pageNumber: number;
	@CollectionOf("T")
	data: T[];
}
