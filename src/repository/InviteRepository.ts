import {Inject, Injectable} from "@tsed/di";
import {MongooseModel} from "@tsed/mongoose";
import {Invite} from "@models/Invite";
import {Repository} from "./Repository";

@Injectable()
export class InviteRepository extends Repository<Invite>{
  @Inject(Invite)
  protected model: MongooseModel<Invite>;
}