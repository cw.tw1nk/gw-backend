import {BodyParams, Post, Req} from "@tsed/common";
import {Controller, Inject} from "@tsed/di";
import {ProjectService} from "@services/ProjectService";
import {Invite} from "@models/Invite";
import {MongooseModel} from "@tsed/mongoose";
import {QuestionRepository} from "@repository/QuestionRepostory";
import {ProjectAnswer} from "@models/Project";

@Controller("/test")
export class TestQuestionnaireCtrl {
    // @Inject(QuestionRepository)
    // qr: QuestionRepository;
    @Inject(ProjectService)
    ps: ProjectService;

    @Inject(Invite)
    private inviteModel: MongooseModel<Invite>;

    @Post("/invite")
    async createProject(@Req() req: Req) {
        // @ts-ignore
        const {session_id} = req.session.user_session;
        return await this.ps.create("6234c88b0f87ddd3ede66c4f", session_id);
        // return await this.qs.findAnswer("6241d384fe542dc699c12f7a");
    }

    @Post("/answer")
    async answer(@BodyParams("project_id") project_id: string, @BodyParams("answer") answer: Omit<ProjectAnswer, "_id" | "is_valid">) {
        return await this.ps.saveAnswer(project_id, answer);
        // return await this.qs.findAnswer("6241d384fe542dc699c12f7a");
    }
}
