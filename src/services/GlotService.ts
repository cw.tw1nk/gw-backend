import Axios, { AxiosInstance } from "axios";
import { Injectable } from "@tsed/di";
import { Value } from "@tsed/common";

export enum GlotLanguage {
	ASSEMBLY="assembly",
	BASH="bash",
	PYTHON="python",
}

@Injectable()
export class GlotService{
	@Value('glot_token')
	private glot_token: string

	private http: AxiosInstance;

	$onInit() {
		this.http = Axios.create({
			baseURL: 'https://glot.io/api',
			headers: {'Authorization': `Token ${this.glot_token ?? '20c5bf0f-c2a9-44cc-bd60-5fd1ebd14c34'}`}
		})
	}


	async runCode(code: string, lang: GlotLanguage) {
		return await this.http.post(`/run/${lang}/latest`, {
			"files": [
				{
					"name": "main.py",
					"content": "print(42)"
				}
			]
		});
	}
}