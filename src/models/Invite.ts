import {AdditionalProperties, CollectionOf, Default, Description, Enum, Example, Format, Property} from "@tsed/schema";
import { Model, MongooseIndex, ObjectID, Ref } from "@tsed/mongoose";
import {Questionnaire} from "./questionnaire/Questionnaire";
import {StatusType} from "./StatusType";

// @Schema()
// export class ClassNAme {
// 	@ObjectID()
// 	@Description("Object ID")
// 	@Example("5ce7ad3028890bd71749d477")
// 	_id: string;
// }

@Model({
    indexes: [

    ]
})
@MongooseIndex({expires: 1}, {expireAfterSeconds: 0})
export class Invite {
    @ObjectID()
    @Description("Object ID")
    @Example("5ce7ad3028890bd71749d477")
    _id: string;

    @Ref(Questionnaire)
    questionnaire: Ref<Questionnaire>

    @Property()
    one_off: boolean;

    // @Property()
    // one_off: boolean;

    @Format("date-time")
    expires: Date;

    @Format("date-time")
    @Default(Date.now)
    created_at: Date;

    @Enum(StatusType)
    status: StatusType = StatusType.ACTIVE;
}
